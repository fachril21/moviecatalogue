package com.example.moviecatalogue.ui.favorite.movie

import android.content.Context
import com.example.moviecatalogue.service.local.LocalMovieService

class MovieFavoritePresenter(val movieFavoriteView: MovieFavoriteView.View) :
    MovieFavoriteView.Presenter {

    override fun requestListMovieFavorite(context: Context) {

        val listMovieFavorite = LocalMovieService.getInstance(context).getAllMovie()
        if (listMovieFavorite == null) {
            movieFavoriteView.failureLoadFavorite()
        } else {
            movieFavoriteView.showMovieFavorite(listMovieFavorite)
        }

    }

}