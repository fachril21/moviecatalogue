package com.example.moviecatalogue.ui.favorite.movie


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.ui.detail.movie.MovieDetailActivity
import com.example.moviecatalogue.ui.movie.MovieAdapter
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_movie_favorite.*

/**
 * A simple [Fragment] subclass.
 */
class MovieFavoriteFragment : Fragment(), MovieFavoriteView.View {

    val movieFavoritePresenter = MovieFavoritePresenter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { movieFavoritePresenter.requestListMovieFavorite(it) }

    }

    override fun onResume() {
        super.onResume()
        context?.let { movieFavoritePresenter.requestListMovieFavorite(it) }
    }

    override fun showMovieFavorite(listMovie: ArrayList<Movie>) {
        rv_movie_favorite.layoutManager = LinearLayoutManager(context)
        val movieAdapter = MovieAdapter(listMovie)
        rv_movie_favorite.adapter = movieAdapter

        movieAdapter.setOnItemClickCallback(object : MovieAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Int) {
                showSelectedMovie(data)
            }
        })
    }

    override fun failureLoadFavorite() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.failure_load_movie),
                Toast.LENGTH_LONG,
                true
            ).show()
        }
    }

    private fun showSelectedMovie(movieId: Int) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.EXTRA_DATA, movieId)
        startActivity(intent)
    }


}
