package com.example.moviecatalogue.ui.movie

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.Movie.Movie
import kotlinx.android.synthetic.main.item_list_movie.view.*

class MovieAdapter(private val listMovie: ArrayList<Movie>) :
    RecyclerView.Adapter<MovieAdapter.ViewHolderMovie>() {

    private var onItemClickCallback: MovieAdapter.OnItemClickCallback? = null

    fun setOnItemClickCallback(onItemClickCallback: MovieAdapter.OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderMovie {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_movie, parent, false)
        return ViewHolderMovie(view)
    }

    override fun getItemCount(): Int {
        return listMovie.size
    }

    override fun onBindViewHolder(holder: ViewHolderMovie, position: Int) {
        holder.bind(listMovie[position])
        holder.itemView.setOnClickListener { onItemClickCallback?.onItemClicked(listMovie[position].id) }
    }

    class ViewHolderMovie(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: Movie) {
            with(itemView) {
                Glide.with(itemView.context)
                    .load(
                        String.format(
                            "%s%s",
                            resources.getString(R.string.poster_path),
                            movie.poster_path
                        )
                    )
                    .into(img_movie)
                txt_title.text = movie.title
                txt_description.text = movie.overview
            }
        }
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Int)
    }

    fun getList(): List<Movie> {
        return listMovie
    }

}