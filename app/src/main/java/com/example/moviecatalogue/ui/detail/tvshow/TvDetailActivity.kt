package com.example.moviecatalogue.ui.detail.tvshow

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.service.local.LocalTvShowService
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_tv_detail.*
import kotlin.properties.Delegates

class TvDetailActivity : AppCompatActivity(), TvDetailView.View, View.OnClickListener {


    companion object {
        const val EXTRA_DATA = "extra_data"
    }

//    private var tvShowDatabase: TvShowDatabase? = null

    private var data by Delegates.notNull<Int>()

    private lateinit var tvshowDb: TvShow

    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tv_detail)

        supportActionBar?.hide()

        data = intent.getIntExtra(EXTRA_DATA, 0)

        val tvDetailPresenter = TvDetailPresenter(this, this)
        tvDetailPresenter.requestTvDetail(data)

        btn_back_tv.setOnClickListener(this)
        btn_favorite_tvshow.setOnClickListener(this)
    }

    override fun showTvDetail(tvShow: TvShow) {
        Glide.with(this)
            .load(
                String.format(
                    "%s%s",
                    resources.getString(R.string.poster_path500),
                    tvShow.backdrop_path
                )
            )
            .transform(CenterCrop())
            .into(img_backdrop_tv)
        tv_title_tv.text = tvShow.title
        tv_rating_tv.text = tvShow.vote_average
        tv_release_tv.text = tvShow.release_date
        tv_overview_tv.text = tvShow.overview
        progressBar_tv.visibility = View.GONE
        detail_layout_tv.visibility = View.VISIBLE
        this.tvshowDb = tvShow
    }

    override fun isFavorite() {
        btn_favorite_tvshow.text = resources.getString(R.string.delete_to_favorite_tvshow)
        isFavorite = true
    }

    override fun failureLoad() {
        Toasty.warning(
            this,
            resources.getString(R.string.failure_load_tvshow),
            Toast.LENGTH_LONG,
            true
        ).show()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back_tv -> {
                onBackPressed()
            }
            R.id.btn_favorite_tvshow -> {
                if (isFavorite) {
                    deleteFromDb(tvshowDb)
                } else {
                    insertToDb(tvshowDb)
                }
            }
        }
    }

    private fun insertToDb(tvShow: TvShow) {
        LocalTvShowService.getInstance(this).insert(tvShow)
        Toasty.success(
            this,
            resources.getString(R.string.added_to_favorite),
            Toast.LENGTH_LONG,
            true
        ).show()
        btn_favorite_tvshow.text = resources.getString(R.string.delete_to_favorite_tvshow)
        isFavorite = true
    }

    private fun deleteFromDb(tvShow: TvShow) {
        LocalTvShowService.getInstance(this).delete(tvShow)
        Toasty.success(
            this,
            resources.getString(R.string.deleted_from_favorite),
            Toast.LENGTH_LONG,
            true
        ).show()
        btn_favorite_tvshow.text = resources.getString(R.string.add_to_favorite_tvshow)
        isFavorite = false
    }
}
