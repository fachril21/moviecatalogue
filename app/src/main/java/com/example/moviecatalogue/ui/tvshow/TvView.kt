package com.example.moviecatalogue.ui.tvshow

import com.example.moviecatalogue.model.TvShow.TvShow

interface TvView {
    interface View {
        fun showTv(listTv: ArrayList<TvShow>)
        fun failureLoad()
        fun emptySearch()
    }

    interface Presenter {
        fun requestListTv()
        fun searchTvShow(query: String)
    }
}