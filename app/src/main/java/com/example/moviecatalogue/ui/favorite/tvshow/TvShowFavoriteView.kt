package com.example.moviecatalogue.ui.favorite.tvshow

import android.content.Context
import com.example.moviecatalogue.model.TvShow.TvShow

interface TvShowFavoriteView {
    interface View {
        fun showTvShowFavorite(listTvShow: ArrayList<TvShow>)
        fun failureLoadFavorite()
    }

    interface Presenter {
        fun requestListTvShowFavorite(context: Context)
    }
}