package com.example.moviecatalogue.ui.detail.movie

import com.example.moviecatalogue.model.Movie.Movie

interface MovieDetailView {
    interface View {
        fun showMovieDetail(movie: Movie)
        fun failureLoad()
        fun isFavorite()
    }

    interface Presenter {
        fun requestMovieDetail(movieId: Int)
    }
}