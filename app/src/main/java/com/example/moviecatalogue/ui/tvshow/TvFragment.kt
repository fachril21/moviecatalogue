package com.example.moviecatalogue.ui.tvshow


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.ui.detail.tvshow.TvDetailActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_tv.*

/**
 * A simple [Fragment] subclass.
 */
class TvFragment : Fragment(), TvView.View, SearchView.OnQueryTextListener {



    val tvPresenter = TvPresenter(this)

    private val TV_KEY: String = "Tv"
    private var listTv: ArrayList<TvShow> = arrayListOf()

    private var ctx: Context? = null
    private var activity: Activity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            showTv(savedInstanceState.getParcelableArrayList<TvShow>(TV_KEY) as ArrayList<TvShow>)
            return
        }

        tvPresenter.requestListTv()
        et_search_tvshow.setOnQueryTextListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(TV_KEY, listTv)
    }

    override fun showTv(listTv: ArrayList<TvShow>) {
        this.listTv = listTv
        rv_tv.layoutManager = LinearLayoutManager(context)
        val tvAdapter = TvAdapter(listTv)
        rv_tv.adapter = tvAdapter
        progressBar.visibility = View.GONE
        tvAdapter.setOnItemClickCallback(object : TvAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Int) {
                showSelectedTv(data)
            }

        })
    }

    override fun failureLoad() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.failure_load_movie),
                Toast.LENGTH_LONG,
                true
            ).show()
        }

    }

    override fun emptySearch() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.empty_tvshow_search),
                Toast.LENGTH_LONG,
                true
            ).show()
        }
    }

    private fun showSelectedTv(tvId: Int) {
        val intent = Intent(context, TvDetailActivity::class.java)
        intent.putExtra(TvDetailActivity.EXTRA_DATA, tvId)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let { tvPresenter.searchTvShow(it) }
        return false
    }

    override fun onQueryTextChange(query: String?): Boolean {
        query?.let { tvPresenter.searchTvShow(it) }
        return false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.ctx = context
        this.activity = getActivity()
    }


}
