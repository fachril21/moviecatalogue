package com.example.moviecatalogue.ui.favorite


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.moviecatalogue.R
import com.example.moviecatalogue.ui.favorite.movie.MovieFavoriteFragment
import com.example.moviecatalogue.ui.favorite.tvshow.TvShowFavoriteFragment
import kotlinx.android.synthetic.main.fragment_favorite.*

/**
 * A simple [Fragment] subclass.
 */
class FavoriteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val favoritePagerAdapter = FavoritePagerAdapter(fragmentManager)
        favoritePagerAdapter.addFragment(
            MovieFavoriteFragment(),
            resources.getString(R.string.title_movie_favorite)
        )
        favoritePagerAdapter.addFragment(
            TvShowFavoriteFragment(),
            resources.getString(R.string.title_tvshow_favorite)
        )


        view_pager_favorite.adapter = favoritePagerAdapter
        tabs_favorite.setupWithViewPager(view_pager_favorite)
        (activity as AppCompatActivity).supportActionBar?.elevation = 0f
    }


}
