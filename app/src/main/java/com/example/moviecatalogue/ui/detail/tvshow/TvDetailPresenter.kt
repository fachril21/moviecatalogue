package com.example.moviecatalogue.ui.detail.tvshow

import android.content.Context
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.service.api.ApiService
import com.example.moviecatalogue.service.local.LocalTvShowService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TvDetailPresenter(var tvDetailView: TvDetailView.View, val context: Context) :
    TvDetailView.Presenter {
    override fun requestTvDetail(tvId: Int) {

        ApiService.instance.getDetailTvShow(tvId).enqueue(object : Callback<TvShow> {
            override fun onFailure(call: Call<TvShow>, t: Throwable) {
                tvDetailView.failureLoad()
            }

            override fun onResponse(call: Call<TvShow>, response: Response<TvShow>) {
                var tvObject = response.body()
                tvObject?.let { isFavorite(it) }
                tvObject?.let { tvDetailView.showTvDetail(it) }
            }

        })
    }

    fun isFavorite(tvObject: TvShow) {
        var list = LocalTvShowService.getInstance(context).getById(tvObject.id)
        if (list != null) {
            if (list.size >= 1) {
                tvDetailView.isFavorite()
            }
        }
    }

}