package com.example.moviecatalogue.ui.movie


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.ui.detail.movie.MovieDetailActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_movie.*

/**
 * A simple [Fragment] subclass.
 */
class MovieFragment : Fragment(), MovieView.View, SearchView.OnQueryTextListener{



    val moviePresenter = MoviePresenter(this)

    private val MOVIE_KEY: String = "Movie"
    private var listMovie: ArrayList<Movie> = arrayListOf()
    private var ctx: Context? = null
    private var activity: Activity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null) {
            showMovie(savedInstanceState.getParcelableArrayList<Movie>(MOVIE_KEY) as ArrayList<Movie>)
            return
        }

        moviePresenter.requestListMovie()


        et_search_movie.setOnQueryTextListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(MOVIE_KEY, listMovie)
    }

    override fun showMovie(listMovie: ArrayList<Movie>) {
        this.listMovie = listMovie
        rv_movie.layoutManager = LinearLayoutManager(context)
        val movieAdapter = MovieAdapter(listMovie)
        rv_movie.adapter = movieAdapter
        progressBar.visibility = View.GONE

        movieAdapter.setOnItemClickCallback(object : MovieAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Int) {
                showSelectedMovie(data)
            }

        })
    }

    override fun failureLoad() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.failure_load_movie),
                Toast.LENGTH_LONG,
                true
            ).show()
        }
    }

    override fun emptySearch() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.empty_movie_search),
                Toast.LENGTH_LONG,
                true
            ).show()
        }
    }

    private fun showSelectedMovie(movieId: Int) {
        val intent = Intent(context, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.EXTRA_DATA, movieId)
        startActivity(intent)
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.let { moviePresenter.searchMovie(it) }
        return false
    }

    override fun onQueryTextChange(query: String?): Boolean {
        query?.let { moviePresenter.searchMovie(it) }
        return false
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.ctx = context
        this.activity = getActivity()

    }

}

