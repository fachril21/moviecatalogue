package com.example.moviecatalogue.ui.favorite.tvshow

import android.content.Context
import com.example.moviecatalogue.service.local.LocalTvShowService

class TvShowFavoritePresenter(var tvShowFavoriteView: TvShowFavoriteView.View) :
    TvShowFavoriteView.Presenter {
    override fun requestListTvShowFavorite(context: Context) {

        val listTvShow = LocalTvShowService.getInstance(context).getAllTvShow()
        if (listTvShow == null) {
            tvShowFavoriteView.failureLoadFavorite()
        } else {
            tvShowFavoriteView.showTvShowFavorite(listTvShow)
        }

    }

}