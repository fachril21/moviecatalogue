package com.example.moviecatalogue.ui.detail.tvshow

import com.example.moviecatalogue.model.TvShow.TvShow

interface TvDetailView {
    interface View {
        fun showTvDetail(tvShow: TvShow)
        fun failureLoad()
        fun isFavorite()
    }

    interface Presenter {
        fun requestTvDetail(tvId: Int)
    }
}