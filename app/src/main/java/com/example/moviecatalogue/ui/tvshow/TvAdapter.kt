package com.example.moviecatalogue.ui.tvshow

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.TvShow.TvShow
import kotlinx.android.synthetic.main.item_list_tv.view.*

class TvAdapter(private val listTvShow: ArrayList<TvShow>) :
    RecyclerView.Adapter<TvAdapter.ViewHolderTvShow>() {
    private var onItemClickCallback: TvAdapter.OnItemClickCallback? = null

    fun setOnItemClickCallback(onItemClickCallback: TvAdapter.OnItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderTvShow {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_list_tv, parent, false)
        return ViewHolderTvShow(view)
    }

    override fun getItemCount(): Int {
        return listTvShow.size
    }

    override fun onBindViewHolder(holder: ViewHolderTvShow, position: Int) {
        holder.bind(listTvShow[position])
        holder.itemView.setOnClickListener { onItemClickCallback?.onItemClicked(listTvShow[position].id) }
    }

    class ViewHolderTvShow(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(tvShow: TvShow) {
            with(itemView) {
                Glide.with(itemView.context)
                    .load(
                        String.format(
                            "%s%s",
                            resources.getString(R.string.poster_path),
                            tvShow.poster_path
                        )
                    )
                    .into(img_tvShow)
                txt_title.text = tvShow.title
                txt_description.text = tvShow.overview
            }
        }
    }

    interface OnItemClickCallback {
        fun onItemClicked(data: Int)
    }

}