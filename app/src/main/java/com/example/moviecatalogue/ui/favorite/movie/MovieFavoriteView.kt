package com.example.moviecatalogue.ui.favorite.movie

import android.content.Context
import com.example.moviecatalogue.model.Movie.Movie

interface MovieFavoriteView {
    interface View {
        fun showMovieFavorite(listMovie: ArrayList<Movie>)
        fun failureLoadFavorite()
    }

    interface Presenter {
        fun requestListMovieFavorite(context: Context)
    }
}