package com.example.moviecatalogue.ui.detail.movie

import android.appwidget.AppWidgetManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.service.local.AppDatabase
import com.example.moviecatalogue.service.local.LocalMovieService
import com.example.moviecatalogue.ui.widget.FavoriteMovieWidget
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlin.properties.Delegates

class MovieDetailActivity : AppCompatActivity(), MovieDetailView.View, View.OnClickListener {


    companion object {
        const val EXTRA_DATA = "extra_data"
    }

    private var appDatabase: AppDatabase? = null

    private var data by Delegates.notNull<Int>()

    private lateinit var movieDb: Movie

    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        supportActionBar?.hide()

        data = intent.getIntExtra(EXTRA_DATA, 0)

        val movieDetailPresenter = MovieDetailPresenter(this, this)

        movieDetailPresenter.requestMovieDetail(data)

        btn_back.setOnClickListener(this)
        btn_favorite_movie.setOnClickListener(this)
    }

    override fun showMovieDetail(movie: Movie) {
        Glide.with(this)
            .load(
                String.format(
                    "%s%s",
                    resources.getString(R.string.poster_path500),
                    movie.backdrop_path
                )
            )
            .transform(CenterCrop())
            .into(img_backdrop)
        tv_title.text = movie.title
        tv_rating.text = movie.vote_average
        tv_release.text = movie.release_date
        tv_overview.text = movie.overview
        progressBar.visibility = View.GONE
        detail_layout.visibility = View.VISIBLE
        this.movieDb = movie
    }

    override fun failureLoad() {
        Toasty.warning(
            this,
            resources.getString(R.string.failure_load_movie),
            Toast.LENGTH_LONG,
            true
        ).show()
    }

    override fun isFavorite() {
        btn_favorite_movie.text = resources.getString(R.string.delete_to_favorite_movie)
        isFavorite = true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_back -> {
                onBackPressed()
            }
            R.id.btn_favorite_movie -> {
                if (isFavorite) {
                    deleteFromDb(movieDb)
                } else {
                    insertToDb(movieDb)
                }
            }
        }
    }

    private fun insertToDb(movie: Movie) {
        LocalMovieService.getInstance(this).insert(movie)
        Toasty.success(
            this,
            resources.getString(R.string.added_to_favorite),
            Toast.LENGTH_LONG,
            true
        ).show()
        btn_favorite_movie.text = resources.getString(R.string.delete_to_favorite_movie)

        val intent = Intent(this, FavoriteMovieWidget::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        val ids = AppWidgetManager.getInstance(application).getAppWidgetIds(ComponentName(applicationContext, FavoriteMovieWidget::class.java))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(intent)

        isFavorite = true
    }

    private fun deleteFromDb(movie: Movie) {
        LocalMovieService.getInstance(this).delete(movie)
        Toasty.success(
            this,
            resources.getString(R.string.deleted_from_favorite),
            Toast.LENGTH_LONG,
            true
        ).show()
        btn_favorite_movie.text = resources.getString(R.string.add_to_favorite_movie)

        val intent = Intent(this, FavoriteMovieWidget::class.java)
        intent.action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
        val ids = AppWidgetManager.getInstance(application).getAppWidgetIds(ComponentName(applicationContext, FavoriteMovieWidget::class.java))
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids)
        sendBroadcast(intent)

        isFavorite = false
    }
}
