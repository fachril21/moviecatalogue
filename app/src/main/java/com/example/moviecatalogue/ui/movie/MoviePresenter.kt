package com.example.moviecatalogue.ui.movie

import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.model.Movie.MovieResponse
import com.example.moviecatalogue.service.api.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class MoviePresenter(val movieView: MovieView.View) : MovieView.Presenter {
    override fun searchMovie(query: String) {
        if (query.isEmpty()){
            requestListMovie()
            return
        }
        ApiService.instance.getMovieSearchResult(query).enqueue(object : Callback<MovieResponse>{
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                movieView.emptySearch()
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                val movieResponse = response.body()
                val listMovieSearch = movieResponse?.result
                movieView.showMovie(listMovieSearch as ArrayList<Movie>)
            }

        })
    }

    override fun requestListMovie() {

        ApiService.instance.getAllMovie().enqueue(object : Callback<MovieResponse> {
            override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                movieView.failureLoad()
            }

            override fun onResponse(call: Call<MovieResponse>, response: Response<MovieResponse>) {
                val movieResponse = response.body()
                val listMovie = movieResponse?.result
                movieView.showMovie(listMovie as ArrayList<Movie>)
            }

        })
    }

}