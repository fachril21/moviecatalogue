package com.example.moviecatalogue.ui.favorite.tvshow


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.ui.detail.tvshow.TvDetailActivity
import com.example.moviecatalogue.ui.tvshow.TvAdapter
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_tv_show_favorite.*

/**
 * A simple [Fragment] subclass.
 */
class TvShowFavoriteFragment : Fragment(), TvShowFavoriteView.View {

    val tvShowFavoritePresenter = TvShowFavoritePresenter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tv_show_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { tvShowFavoritePresenter.requestListTvShowFavorite(it) }
    }

    override fun onResume() {
        super.onResume()
        context?.let { tvShowFavoritePresenter.requestListTvShowFavorite(it) }
    }

    override fun showTvShowFavorite(listTvShow: ArrayList<TvShow>) {
        rv_tvshow_favorite.layoutManager = LinearLayoutManager(context)
        val tvshowAdapter = TvAdapter(listTvShow)
        rv_tvshow_favorite.adapter = tvshowAdapter

        tvshowAdapter.setOnItemClickCallback(object : TvAdapter.OnItemClickCallback {
            override fun onItemClicked(data: Int) {
                showSelectedTv(data)
            }
        })

    }

    override fun failureLoadFavorite() {
        context?.let {
            Toasty.warning(
                it,
                resources.getString(R.string.failure_load_tvshow),
                Toast.LENGTH_LONG,
                true
            ).show()
        }
    }

    private fun showSelectedTv(tvId: Int) {
        val intent = Intent(context, TvDetailActivity::class.java)
        intent.putExtra(TvDetailActivity.EXTRA_DATA, tvId)
        startActivity(intent)
    }


}
