package com.example.moviecatalogue.ui.widget

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import androidx.core.os.bundleOf
import com.example.moviecatalogue.R
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.service.local.LocalMovieService
import java.io.IOException
import java.net.MalformedURLException
import java.net.URL

internal class StackRemoteViewsFactory(private val context: Context) : RemoteViewsService.RemoteViewsFactory {

    private val BASE_POSTER = "https://image.tmdb.org/t/p/w342"

    private val listMovieItems = ArrayList<Bitmap>()

    override fun onCreate() {

    }

    override fun getLoadingView(): RemoteViews? = null

    override fun getItemId(p0: Int): Long = 0

    override fun onDataSetChanged() {
        listMovieItems.clear()
        for (movie: Movie in LocalMovieService.getInstance(context).getAllMovie()){
            try {
                var url = URL(BASE_POSTER+movie.poster_path)
                listMovieItems.add(BitmapFactory.decodeStream(url.openConnection().getInputStream()))
            } catch (e: MalformedURLException){
                e.printStackTrace()
            } catch (e: IOException){
                e.printStackTrace()
            }
        }
    }

    override fun hasStableIds(): Boolean = false

    override fun getViewAt(position: Int): RemoteViews {
        val rv = RemoteViews(context.packageName, R.layout.widget_movie_item)
        rv.setImageViewBitmap(R.id.iv_widget, listMovieItems[position])
        val extras = bundleOf(
            FavoriteMovieWidget.EXTRA_ITEM to position
        )
        val fillInIntent = Intent()
        fillInIntent.putExtras(extras)
        rv.setOnClickFillInIntent(R.id.iv_widget, fillInIntent)
        return rv
    }

    override fun getCount(): Int = listMovieItems.size

    override fun getViewTypeCount(): Int = 1

    override fun onDestroy() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}