package com.example.moviecatalogue.ui.tvshow

import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.model.TvShow.TvShowResponse
import com.example.moviecatalogue.service.api.ApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class TvPresenter(val tvView: TvView.View) : TvView.Presenter {
    override fun searchTvShow(query: String) {
        if (query.isEmpty()){
            requestListTv()
            return
        }
        ApiService.instance.getTvShowSearchResult(query).enqueue(object : Callback<TvShowResponse>{
            override fun onFailure(call: Call<TvShowResponse>, t: Throwable) {
                tvView.emptySearch()
            }

            override fun onResponse(
                call: Call<TvShowResponse>,
                response: Response<TvShowResponse>
            ) {
                val tvResponse = response.body()
                val listTvSearch = tvResponse?.result
                tvView.showTv(listTvSearch as ArrayList<TvShow>)
            }

        })
    }

    override fun requestListTv() {

        ApiService.instance.getAllTvShow().enqueue(object : Callback<TvShowResponse> {
            override fun onFailure(call: Call<TvShowResponse>, t: Throwable) {
                tvView.failureLoad()
            }

            override fun onResponse(
                call: Call<TvShowResponse>,
                response: Response<TvShowResponse>
            ) {
                val tvResponse = response.body()
                val listTv = tvResponse?.result
                tvView.showTv(listTv as ArrayList<TvShow>)
            }

        })
    }

}