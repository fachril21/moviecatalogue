package com.example.moviecatalogue.ui.detail.movie

import android.content.Context
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.service.api.ApiService
import com.example.moviecatalogue.service.local.LocalMovieService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailPresenter(var movieDetailView: MovieDetailView.View, val context: Context) :
    MovieDetailView.Presenter {
    override fun requestMovieDetail(movieId: Int) {

        ApiService.instance.getDetailMovie(movieId).enqueue(object : Callback<Movie> {
            override fun onFailure(call: Call<Movie>, t: Throwable) {
                movieDetailView.failureLoad()
            }

            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                var movieObject = response.body()
                movieObject?.let { isFavorite(it) }
                movieObject?.let { movieDetailView.showMovieDetail(it) }
            }
        })
    }

    fun isFavorite(movieObject: Movie) {
        var list = LocalMovieService.getInstance(context).getById(movieObject.id)
        if (list != null) {
            if (list.size >= 1) {
                movieDetailView.isFavorite()
            }
        }
    }

}