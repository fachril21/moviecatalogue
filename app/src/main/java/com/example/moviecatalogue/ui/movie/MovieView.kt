package com.example.moviecatalogue.ui.movie

import com.example.moviecatalogue.model.Movie.Movie

interface MovieView {
    interface View {
        fun showMovie(listMovie: ArrayList<Movie>)
        fun failureLoad()
        fun emptySearch()
    }

    interface Presenter {
        fun requestListMovie()
        fun searchMovie(query: String)
    }
}