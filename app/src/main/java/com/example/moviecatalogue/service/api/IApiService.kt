package com.example.moviecatalogue.service.api

import com.example.moviecatalogue.BuildConfig
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.model.Movie.MovieResponse
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.model.TvShow.TvShowResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface IApiService {

//    companion object{
//        private const val API_KEY = "6f2e4062421910291bc3f78d3693aba8"
//    }


    @GET("discover/movie?api_key=" + BuildConfig.API_KEY)
    fun getAllMovie(): Call<MovieResponse>

    @GET("movie/{id}?api_key=" + BuildConfig.API_KEY)
    fun getDetailMovie(@Path("id") movieId: Int): Call<Movie>

    @GET("discover/tv?api_key=" + BuildConfig.API_KEY)
    fun getAllTvShow(): Call<TvShowResponse>

    @GET("tv/{id}?api_key=" + BuildConfig.API_KEY)
    fun getDetailTvShow(@Path("id") tvShowId: Int): Call<TvShow>

    @GET("search/movie?api_key=" + BuildConfig.API_KEY)
    fun getMovieSearchResult(@Query("query") query : String): Call<MovieResponse>

    @GET("search/tv?api_key=" + BuildConfig.API_KEY)
    fun getTvShowSearchResult(@Query("query") query: String): Call<TvShowResponse>

}