package com.example.moviecatalogue.service.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.service.local.dao.MovieDao
import com.example.moviecatalogue.service.local.dao.TvShowDao

@Database(entities = arrayOf(Movie::class, TvShow::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao
    abstract fun tvshowDao(): TvShowDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "showdb"
                    ).allowMainThreadQueries()
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}