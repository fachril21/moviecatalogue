package com.example.moviecatalogue.service.local.dao

import androidx.room.*
import com.example.moviecatalogue.model.TvShow.TvShow

@Dao
interface TvShowDao {
    @Query("SELECT * from tvshow")
    fun getAll(): List<TvShow>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(tvshow: TvShow)

    @Delete
    fun delete(tvshow: TvShow)

    @Query("SELECT * FROM tvshow WHERE id =:tvshowId")
    fun getById(tvshowId: Int): List<TvShow>
}