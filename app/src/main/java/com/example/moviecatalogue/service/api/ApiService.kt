package com.example.moviecatalogue.service.api

import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.model.Movie.MovieResponse
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.model.TvShow.TvShowResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiService {

    private val BASE_URL = "https://api.themoviedb.org/3/"

    private var apiService: IApiService

    companion object {
        val instance = ApiService()
    }

    init {
        val retrofit: Retrofit = createAdapter()
        apiService = retrofit.create(IApiService::class.java)
    }

    fun createAdapter(): Retrofit = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
        GsonConverterFactory.create()
    ).build()

    fun getAllMovie(): Call<MovieResponse> {
        return apiService.getAllMovie()
    }

    fun getDetailMovie(movieId: Int): Call<Movie> {
        return apiService.getDetailMovie(movieId)
    }

    fun getAllTvShow(): Call<TvShowResponse> {
        return apiService.getAllTvShow()
    }

    fun getDetailTvShow(tvShowId: Int): Call<TvShow> {
        return apiService.getDetailTvShow(tvShowId)
    }

    fun getMovieSearchResult(query: String): Call<MovieResponse> {
        return apiService.getMovieSearchResult(query)
    }

    fun getTvShowSearchResult(query: String): Call<TvShowResponse> {
        return apiService.getTvShowSearchResult(query)
    }

}