package com.example.moviecatalogue.service.local

import android.content.Context
import com.example.moviecatalogue.model.TvShow.TvShow
import com.example.moviecatalogue.service.local.dao.TvShowDao

class LocalTvShowService constructor(val context: Context) {

    private var tvshowDao: TvShowDao? = null

    companion object {
        private var instance: LocalTvShowService? = null

        fun getInstance(context: Context): LocalTvShowService {
            if (instance == null) {
                instance = LocalTvShowService(context)
            }
            return instance as LocalTvShowService
        }
    }

    init {
        tvshowDao = AppDatabase.getInstance(context)?.tvshowDao()
    }


    fun getAllTvShow(): ArrayList<TvShow> {
        return tvshowDao?.getAll() as ArrayList<TvShow>
    }

    fun insert(tvShow: TvShow) {
        tvshowDao?.insert(tvShow)
    }

    fun getById(tvshowId: Int): List<TvShow>? {
        return tvshowDao?.getById(tvshowId)
    }

    fun delete(tvShow: TvShow) {
        tvshowDao?.delete(tvShow)
    }
}