package com.example.moviecatalogue.service.local

import android.content.Context
import com.example.moviecatalogue.model.Movie.Movie
import com.example.moviecatalogue.service.local.dao.MovieDao

class LocalMovieService constructor(val context: Context) {

    private var movieDao: MovieDao? = null

    companion object {
        private var instance: LocalMovieService? = null

        fun getInstance(context: Context): LocalMovieService {
            if (instance == null) {
                instance = LocalMovieService(context)
            }
            return instance as LocalMovieService
        }

    }

    init {
        movieDao = AppDatabase.getInstance(context)?.movieDao()
    }


    fun getAllMovie(): ArrayList<Movie> {
        return movieDao?.getAll() as ArrayList<Movie>
    }

    fun insert(movie: Movie) {
        movieDao?.insert(movie)
    }

    fun getById(movieId: Int): List<Movie>? {
        return movieDao?.getById(movieId)
    }

    fun delete(movie: Movie) {
        movieDao?.delete(movie)
    }
}