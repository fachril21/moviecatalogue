package com.example.moviecatalogue.service.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.moviecatalogue.model.Movie.Movie

@Dao
interface MovieDao {
    @Query("SELECT * from movie")
    fun getAll(): List<Movie>

    @Insert(onConflict = REPLACE)
    fun insert(movie: Movie)

    @Delete
    fun delete(movie: Movie)

    @Query("SELECT * FROM movie WHERE id =:movieId")
    fun getById(movieId: Int): List<Movie>
}