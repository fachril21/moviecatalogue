package com.example.moviecatalogue.model.Movie

import com.google.gson.annotations.SerializedName

data class MovieResponse(

    @SerializedName("results")
    var result: List<Movie>
)