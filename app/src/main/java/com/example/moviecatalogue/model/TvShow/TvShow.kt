package com.example.moviecatalogue.model.TvShow

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "tvshow")
data class TvShow(
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey
    var id: Int,

    @SerializedName("original_name")
    @ColumnInfo(name = "title")
    var title: String,

    @SerializedName("overview")
    @ColumnInfo(name = "overview")
    var overview: String,

    @SerializedName("poster_path")
    @ColumnInfo(name = "poster_path")
    var poster_path: String,

    @SerializedName("vote_average")
    @ColumnInfo(name = "vote_average")
    var vote_average: String,

    @SerializedName("first_air_date")
    @ColumnInfo(name = "release_date")
    var release_date: String,

    @SerializedName("backdrop_path")
    @ColumnInfo(name = "backdrop_path")
    var backdrop_path: String
) : Parcelable