package com.example.moviecatalogue.model.TvShow

import com.google.gson.annotations.SerializedName

data class TvShowResponse(
    @SerializedName("results")
    var result: List<TvShow>
)